﻿using UnityEngine;
using System.Collections;

public class MovingObject : MonoBehaviour {

	float speed = 4f;

	// Update is called once per frame
	void Update () {
		
		if (Input.GetKey (KeyCode.D)) {

			//code to move player right
			transform.Translate(Vector2.right * speed * Time.deltaTime);    
		}
		else if (Input.GetKey (KeyCode.A)) {

			//code to move player left
			transform.Translate(-Vector2.right * speed * Time.deltaTime);   
		}
		else if (Input.GetKey (KeyCode.W)) {
			transform.Translate(Vector2.up * speed * Time.deltaTime);
		}
		else if (Input.GetKey (KeyCode.S)) {
			transform.Translate(-Vector2.up * speed * Time.deltaTime);  
		}
	}
}

